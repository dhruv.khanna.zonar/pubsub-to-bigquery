import os
import base64
import json
import http_errors
import tools
from dateutil import tz
from datetime import datetime
from uuid import uuid4

from http_error_handlers import ExternalHttpHandlers
from tools import log_errors
from metrics import stats_initialize
from metrics_labels import MetricsLabels

from lib.big_query import BQEntry, BQSchema, BQPubsubHelper, DATETIME_FORMAT

LOG_LEVEL = os.getenv("LOG_LEVEL")

logging = tools.init_logger("service", LOG_LEVEL)
statsd = stats_initialize()

# BigQuery variables
bq_dataset_id = os.getenv("BQ_DATASET_ID")
bq_hr_table_name = os.getenv("BQ_TABLE_ID")
pubsub_project_id = os.getenv("GCP_PROJECT")
pubsub_topic_id = os.getenv("BQ_PUBSUB_TOPIC_ID")


bq_schema = BQSchema(
    [
        BQEntry(
            "TracingId", is_row_id=True, value_type=["string"], default_value="null"
        ),  # STRING
        BQEntry(
            "StartTimestamp", value_type=["string"], default_value="null"
        ),  # TIMESTAMP
        BQEntry("Vin", value_type=["string"], default_value="null"),  # STRING
        BQEntry(
            "CalibrationStatus", value_type=["int"], default_value="null"
        ),  # INTEGER
        BQEntry("SerialNumber", value_type=["string"], default_value="null"),  # STRING
        
        BQEntry(
            name="ResultRows", value_type=["string", "array"], default_value="null"
        ),  # ARRAY OF STRINGS
        BQEntry(
            "Results",
            value_type=["record"],
            is_record=True,
            fields=[
                {"name": "x", "type": "float"},
                {"name": "y", "type": "float"},
                {"name": "z", "type": "float"},
                {"name": "completion", "type": "int"},
            ],
            default_value={},
        ),  # RECORD
    ]
)

bq_inserter = BQPubsubHelper(
    statsd,
    logging,
    schema=bq_schema,
    pubsub_project=pubsub_project_id,
    pubsub_topic=pubsub_topic_id,
)


# Wrapper only used here for http 5xx errors
@log_errors(logging, statsd, ExternalHttpHandlers, bq_inserter)
def calibration_notification_handler(message, context):
    """Store Harsh Event Data"""
    statsd.distribution(MetricsLabels.INCOMING, 1)

    bq_inserter.set_props(
        {
            "TracingId": str(uuid4()),
            "StartTimestamp": datetime.utcnow().replace(tzinfo=tz.tzutc()).isoformat(),
        }
    )

    try:
        # get the message JSON
        message_bytes = message["data"]
        message_data = base64.b64decode(message_bytes)
        logging.debug("Payload=%s", message_data)

        harsh_message = json.loads(message_data)

        event = harsh_message["event"]
        vin = harsh_message["vin"]
        calibration_status = int(harsh_message["status"])
        serial_number = harsh_message["serial_number"]
        results = harsh_message.get("results")

        if results:
            bq_inserter.set_props({"Results": results})
        else:
            bq_inserter.set_props({"Results": {}})

        bq_inserter.set_props(
            {
                "Event": event,
                "Vin": vin,
                "CalibrationStatus": calibration_status,
                "SerialNumber": serial_number,
            }
        )
        logging.debug(
            "Setting properties for bq_inserter: event=%s, vin=%s, calibration_status=%s, serial_number=%s",
            event,
            vin,
            calibration_status,
            serial_number,
        )

        bq_inserter.insert()

        return "Calibration successfully wrote to BigQuery", 200
    except http_errors.HTTPError500:
        # wrapper handles errors
        raise
    except http_errors.HTTPError as e:
        logging.error("%s", str(e))
        code_tag = http_errors.HTTPError.get_http_status_code_tag(e)
        statsd.distribution(MetricsLabels.ERRORS, 1, tags=["type:nonfatal"] + code_tag)
        # CRVD-238 Detailed+Design+>>+errors+management
        # return 200 in case of http_error other than 500
        return e.message, 200
    except Exception as ex:
        logging.error("[code 500] unexpected error: %s", str(ex))
        raise http_errors.HTTPError500()
