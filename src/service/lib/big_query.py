"""
A module that helps to collect information and insert to Big Query
This module supports 2 insert method: 
- Using BQHelper class, the record will be inserted directly to BigQuery table using BigQuery Client
- Using BQPubsubHelper class, the record will be published to a pubsub topic using Pubsub Publisher Client, 
and inserted to BigQuery table by a subscription assocciate with that topic (aka BQ subscription)
"""
import json
import time
from logging import Logger
from typing import Dict, List

from dateutil import parser
from datadog.dogstatsd.base import DogStatsd
from datadog import ThreadStats
from google.cloud import bigquery
from google import api_core
from google.cloud.pubsub_v1 import PublisherClient

from exceptions import OemLibException
from metrics_tags import REASON_KEYWORD

import avro.io
from avro.schema import SchemaFromJSONData as make_avsc_object
from avro_json_serializer import AvroJsonSerializer

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S"

SUPPORTED_AVRO_TYPES = [
    "string",
    "bytes",  # string types
    "int",
    "long",
    "float",
    "double",  # number types
    "boolean",  # boolean type
    "null",
    "array",
    "record",
]  # null type


class BQEntry:
    """
    BigQuery Entry object
    """

    def __init__(
        self,
        name,
        default_value=None,
        value_type: list = None,
        use_set_prop=True,
        is_row_id=False,
        is_null_value_allowed=True,
        is_record=False,
        fields=[],
    ):
        """
        BigQuery Entry object

        :param name: entry name
        :param default_value: default value of the entry (must be set if we use BigQuery Pubsub)
        :param value_type: list of possible types of the entry value (must be set if we use BigQuery Pubsub)
        :param use_set_prop: flag entry as "set property" - defaults to True
        :param is_row_id: flag entry as the row ID. (There can only be one!) - defaults to False
        :param is_null_value_allowed: flag for allowing to set null value (including "null" "None")
        """
        self.name = name
        self.default_value = default_value
        self.value_type = value_type
        self.use_set_prop = use_set_prop
        self.is_row_id = is_row_id
        self.is_null_value_allowed = is_null_value_allowed
        self.is_record = is_record
        self.fields = fields


class BQSchema:
    """
    Schema of the BigQuery payload
    """

    def __init__(self, bq_entries: List[BQEntry]):
        """
        Initialize BiqQuery Schema

        :param bq_entries: list of BQ entries
        """
        self._bq_entries = bq_entries
        self._schema = None
        self._is_valid = None
        self.key_set = set()
        self.keys_where_set_prop_is_not_allowed = set()
        self.row_key = None
        self.keys_where_null_is_not_allowed = set()
        # An array of dict with key is the name of entry that can have null type,
        # this array will be used to create avro schema and pubsub payload for BQ Pubsub helper.
        self.keys_with_optional_null_type = []
        self._avro_schema = None
        self._is_value_types_available = False

    def validate_schema_errors(self) -> List[str]:
        """
        Validation method which returns a list of errors if there are validation errors.
        If returned list is empty, then the schema is valid

        :return: a list of errors if there are validation errors, else empty list
        """
        _schema_errors = []
        _schema_keys = set()
        _row_key = None

        # make sure the valid schema is validated only once
        if self._is_valid:
            return _schema_errors

        for entry in self._bq_entries:
            # make this check valid from the begining of validation, if there is error, it will be false
            self._is_valid = True

            # Check if the given entry is not record and if it has fields
            if entry.is_record == False and len(entry.fields) > 0:
                self._is_valid = False
                _schema_errors.append(
                    f"BQ Schema Error: entry key {entry.name} is not a record"
                )

            # Check if the given entry is a record and has valid fields
            if entry.is_record == True and len(entry.fields) == 0:
                self._is_valid = False
                _schema_errors.append(
                    f"BQ Schema Error: entry key {entry.name} is a record but has no fields"
                )

            # check for duplicate entry keys
            if entry.name in _schema_keys:
                self._is_valid = False
                _schema_errors.append(
                    f"BQ Schema Error: we have entry key {entry.name} already"
                )

            # check for duplicate row ids
            if entry.is_row_id:
                if _row_key is None:
                    _row_key = entry.name
                else:
                    self._is_valid = False
                    _schema_errors.append(
                        f"BQ Schema Error: we already have a row key "
                        f"{_row_key}. We can't use {entry.name} as a row key"
                    )

            # add key for dedupe
            _schema_keys.add(entry.name)

            # check for value type of entry
            if entry.value_type:
                self._is_value_types_available = True
                # check the value in the list of types is avro supported or not
                if not set(entry.value_type).issubset(set(SUPPORTED_AVRO_TYPES)):
                    self._is_valid = False
                    _schema_errors.append(
                        f"BQ Schema Error: List of valid types '{entry.value_type}' of \
                        entry '{entry.name}' contains invalid type"
                    )
                # check if null type is in value type if default value is None or not
                if entry.default_value is None and "null" not in entry.value_type:
                    self._is_valid = False
                    _schema_errors.append(
                        f"BQ Schema Error: Default value '{entry.default_value}' of entry '{entry.name}' \
                        is not any of type in {entry.value_type}"
                    )

                # check if there is null in entry value type list

                if "null" in entry.value_type:

                    if "record" in entry.value_type or "array" in entry.value_type:
                        _schema_errors.append(
                            f"BQ Schema Error: Entry '{entry.name}' is of type record and is not allowed to have null value"
                        )

                    if not entry.is_null_value_allowed:
                        _schema_errors.append(
                            f"BQ Schema Error: Entry '{entry.name}' is not allowed to have null value"
                        )
                    else:
                        # add a dict with entry name and its valid type to optional null type list
                        # make sure the member in the "type" array is in order ["null", <valid-type>]
                        self.keys_with_optional_null_type.append(
                            {
                                "name": entry.name,
                                "type": [
                                    entry.value_type[0]
                                    if entry.value_type[0] == "null"
                                    else entry.value_type[1],
                                    entry.value_type[0]
                                    if entry.value_type[0] != "null"
                                    else entry.value_type[1],
                                ],
                            }
                        )
                if "record" in entry.value_type or "array" in entry.value_type:
                    # add a dict with entry name and its valid type to null_is_not_allowed type list
                    # make sure the member in the "type" array is in order ["array", <valid-type>]
                    self.keys_where_null_is_not_allowed.add(entry.name)

            # make sure all entries are added with value_type if there is one has been set
            # otherwise, avro schema cannot be created correctly
            if self._is_value_types_available and not entry.value_type:
                self._is_valid = False
                _schema_errors.append(
                    f"BQ Schema Error: Entry '{entry.name}' must be init with value_type in this schema"
                )

        # Check that we have exactly one row key
        if _row_key is None:
            self._is_valid = False
            _schema_errors.append("BQ Schema Error: we have no row key")

        return _schema_errors

    def _load_schema(self) -> Dict:
        """
        NOTE: This is assuming the validate-schema-errors method is called first.
        Returns the schema as a dictionary.

        :return: our schema as a dictionary.
        """
        _schema = {}
        for entry in self._bq_entries:
            # Update the entry to the overall schema
            _schema.update({entry.name: entry.default_value})

            if not entry.use_set_prop:
                self.keys_where_set_prop_is_not_allowed.add(entry.name)

            if entry.is_row_id:
                self.row_key = entry.name

            if not entry.is_null_value_allowed:
                self.keys_where_null_is_not_allowed.add(entry.name)

        self.key_set = set(_schema.keys())

        return _schema

    @property
    def schema(self) -> Dict:
        """
        This is assuming the validate-schema-errors method is called first.
        Lazy-loads the schema as a dictionary.

        :return: our schema as a dictionary.
        """
        if not self._schema:
            self._schema = self._load_schema()

        return self._schema

    def _load_avro_schema(self) -> Dict:
        """
        NOTE: This is assuming the validate_schema_errors method is called first.
        Returns the validator schema as a dictionary.

        :return: our validator schema as a dictionary.
        """
        _avro_schema = {"type": "record", "name": "Avro"}
        _fields = []

        for entry in self._bq_entries:
            # Check if the entry has optional null type or not for creating a valid avro schema
            entry_with_null = [
                current_entry
                for current_entry in self.keys_with_optional_null_type
                if current_entry["name"] is entry.name
            ]
            print(entry_with_null)

            if not entry_with_null:
                if "record" in entry.value_type:
                    _fields.append(
                        {
                            "name": entry.name,
                            "type": {
                                "type": "record",
                                "name": entry.name,
                                "fields": entry.fields,
                            },
                        }
                    )

                elif "array" in entry.value_type:
                    _fields.append(
                        {
                            "name": entry.name,
                            "type": {"type": "array", "items": entry.value_type[0],},
                        }
                    )
                else:
                    _fields.append({"name": entry.name, "type": entry.value_type[0]})

            else:
                if "array" in entry.value_type:
                    _fields.append(
                        {
                            "name": entry.name,
                            "type": {"type": "array", "items": entry.value_type[0],},
                        }
                    )
                else:
                    _fields.append(entry_with_null[0])

        _avro_schema.update({"fields": _fields})

        return _avro_schema

    @property
    def avro_schema(self) -> Dict:
        """
        This is assuming value type for each entry is available and valid
        Returns the validator schema as a dictionary.

        :return: our validator schema as a dictionary if there is value type available, otherwise None
        """
        if not self._is_value_types_available:
            return None
        if not self._avro_schema:
            self._avro_schema = self._load_avro_schema()

        return self._avro_schema


class BQException(OemLibException):
    """An error for exporting to BigQuery."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class BQHelper:
    """
    A class that helps to collect information and insert to Big Query

    ## Awareness
    This helper is not threading-safe,
    so if you are in development with service using multiple threading,
    acknowledge this can help you save your time.

    ## Usage
    ```python
    # Import modules
    from oem_lib.big_query import BQHelper, BQSchema, BQEntry

    # Define schema and default value
    # NOTE: This is schema is used to gather up information only
    # Please create you `dataset.table` manualy (or terraform)
    # with predefined schema that match with this one
    # NOTE: If you don't want a entry to have null value (including "null", "None") as its value,
    # you can turn off the flag is_null_value_allowed=False
    schema = BQSchema([
        BQEntry("TracingId", is_row_id=True),
        BQEntry("StatusCode"),
        BQEntry("ErrorMessage")
    ])

    # Check for validation errors
    schema_errors = schema.validate_schema_errors
    if schema_errors
        raise BQException(" / ".join(schema_errors))

    # Init BQHelper instance
    #
    # NOTE: the BQSchema param is validated when initiating the BQHelper. If the schema is invalid,
    # the BQHelper instance will be automatically flagged as disabled. The methods can still
    # be called, but they will do nothing. It's best to explicitly validate the schema before
    # we reach this point, though

    bq = BQHelper(statsd, logger, "dataset_id", "table_id", schema)
    NOTE: "statsd" can be set to None safely

    # Set props and insert to BigQuery
    bq.set_prop("TracingId", "abc-def-ghi")
    bq.set_props({
        "StatusCode": "200",
        "ErrorMessage": None
    })
    bq.insert()

    # Or a shortcut
    bq.insert(props={
        "TracingId": "abc-def-ghi",
        "StatusCode": "200",
        "ErrorMessage": None
    })

    # Or if you want to use Log Router and Logging to export logs from Stackdriver to Bigquery:
    import logging
    logger = logging.getLogger('your.service.name')
    logging.info("Insert new record to BigQuery", extra=bq.bq_dict)
    ```
    """

    def __init__(
        self,
        statsd: [DogStatsd, None],
        logger: Logger,
        dataset_id: str,
        table_id: str,
        schema: BQSchema,
        enabled: bool = True,
        bq_client=None,
    ) -> None:
        # Logging and statsd
        self.logger: Logger = logger
        self.statsd = statsd

        # Big Query client. Create a client if none is provided.
        self.client = bq_client if bq_client else bigquery.Client()

        # Get target table id and schema if bq client is used. In case of BQ Pubsub helper, it is not used.
        if isinstance(self.client, (str, bool)):
            self.table_id = None
        else:
            self.table_id = f"{self.client.project}.{dataset_id}.{table_id}"

        # Constants
        self.NUM_OF_RETRY = 5
        self.SLEEP_DUETO_RETRY = 1

        self.schema = schema
        self.init_default_field_values(enabled)

    def _validate_load_schema(self, schema: BQSchema):
        """
        Set schema and validate it. If invalid, disable this helper
        """
        _schema_errors = schema.validate_schema_errors()
        if _schema_errors:
            err_str = (
                f"Provided BQ Schema has errors: {' / '.join(_schema_errors)}. "
                f"Disabling BQ Helper."
            )
            self.logger.error(err_str)
            if self.statsd:
                self.statsd.increment(
                    "errors", tags=["source:bq", "reason:invalid_schema"]
                )

            self.enabled = False
            self.bq_dict = {}
            return False
        return True

    def init_default_field_values(self, enabled=True):
        """
        Init default value for the helper collector

        Should be called to clearing gathered values after inserting.
        """
        self.bq_dict = {}
        if self._validate_load_schema(self.schema):
            self.bq_dict = {**self.schema.schema}
            self.row_key = self.schema.row_key
            self.enabled = enabled

    @property
    def bq_client(self):
        """
        Get the bq client
        """
        return self.client

    def set_prop(self, key, value):
        """
        Set a key value to current payload
        Only set the value to the coresponding key if key name is allowed

        :param key:
        :param value:
        """
        if not self.enabled:
            return

        if key in self.schema.keys_where_set_prop_is_not_allowed:
            self.logger.error(f"{key} is not allowed to set via set_props method")
            if self.statsd:
                self.statsd.increment(
                    "errors", tags=["source:bq", "reason:invalid_schema"]
                )
            return
        if key not in self.schema.key_set:
            self.logger.error(f"Unknown key {key}")
            if self.statsd:
                self.statsd.increment(
                    "errors", tags=["source:bq", "reason:invalid_schema"]
                )
            return
        self.bq_dict = {**self.bq_dict, **{key: value}}

    def set_props(self, key_value_pairs: Dict):
        """
        Set multiple key value to current payload
        Only set the value to the coresponding key if key name is allowed

        :param key_value_pairs: The dict presents what fields need to add
        """
        if not self.enabled:
            return

        for key, value in key_value_pairs.items():
            self.set_prop(key, value)

    @staticmethod
    def _determine_retry(errors):
        """
        We only retry this if we have a non invalid error
        """
        NONE_RETRY_REASON = "invalid"

        reason = errors[0]["errors"][0]["reason"].lower()

        is_retryed = reason not in NONE_RETRY_REASON

        return is_retryed, reason

    def _logging_info(self, message, errors):
        """
        Get logging payload for full information
        """
        return {"message": message, "errors": errors, "payload": self.bq_dict}

    @staticmethod
    def format_datetime(dt_string):
        """
        Removing tz from timestamp
        """
        ts = parser.parse(dt_string).replace(tzinfo=None)
        return ts.strftime(DATETIME_FORMAT)

    def is_null_values_valid(self) -> bool:
        """
        Check keys in set of keys where null is not allow have null value or not
        in the bq_dict (including None, "null", "None")

        :return: False if there is invalid null value set. If everything is ok, return True.
        """
        result = True
        # if the list is empty, return immediately
        if not self.schema.keys_where_null_is_not_allowed:
            return result

        for key in self.schema.keys_where_null_is_not_allowed:
            if self.bq_dict.get(key) in [None, "None", "null"]:
                self.logger.error(
                    f"{key} is not allowed to have null value, aborting BigQuery insert"
                )
                result = False

        return result

    def insert(self, props=None) -> [Dict, None]:
        """
        If the helper is disabled, just clearing collected fields

        Insert our current payload (and additional properties specified here) into BiqQuery Data

        If the insert is successful, we will return the payload representing what was inserted, and
        we will reset the payload to its default state. Otherwise, None is returned, which
        indicates something went wrong.

        :param props: an additional dictionary of key/value pairs to insert if specified -- defaults
               to no properties
        :return: payload inserted into BigQuery on success, or None if something went wrong, or
                 this helper is disabled
        """
        if not self.enabled:
            self.init_default_field_values()
            return None

        try:
            if props:
                self.set_props(props)

            if not self.is_null_values_valid():
                return None

            insert_errors = []
            for try_num in range(self.NUM_OF_RETRY):
                insert_errors = self.bq_client.insert_rows_json(
                    table=self.table_id, json_rows=[self.bq_dict]
                )

                if not insert_errors:
                    # Insert successfully
                    msg = "Insert successfully to BigQuery"
                    self.logger.debug(self._logging_info(msg, None))
                    return self.bq_dict

                is_retryable, reason = self._determine_retry(insert_errors)
                if not is_retryable:
                    msg = f"Dropping this record due to {reason}. Detail: {json.dumps(insert_errors)}"
                    raise BQException(msg, **{REASON_KEYWORD: "bq_invalid_or_drop"})

                # Retry
                msg = f"Retrying to insert record to Big Query: {try_num + 1} time(s)."
                self.logger.warning(self._logging_info(msg, insert_errors))
                time.sleep(self.SLEEP_DUETO_RETRY)

            msg = (
                f"Hit retry limit ({self.NUM_OF_RETRY} tries) when inserting data to BQ"
            )
            raise BQException(msg, **{REASON_KEYWORD: "bq_retry_limit"})
        except BQException as error:
            msg = "There was an BQException when publishing message to BigQuery"
            self.logger.error(self._logging_info(msg, str(error)), exc_info=error)
            if self.statsd:
                self.statsd.increment(
                    "errors", tags=error.generate_tags() + ["source:bq"]
                )
            return None
        except Exception as error:
            msg = "There was an error when publishing message to BigQuery"
            self.logger.error(self._logging_info(msg, str(error)), exc_info=error)
            if self.statsd:
                self.statsd.increment("errors", tags=["source:bq", "reason:unknown"])
            return None
        finally:
            self.init_default_field_values()


class BQInserter:
    """
    A class that will help manage common operations for muliple BQHelper instances
    """

    def __init__(
        self, stats: ThreadStats, logger: Logger, bq_client: bigquery.Client = None,
    ):
        """
        Initialize BQInserter

        :param stats: ThreadStats object for Datadog metrics gathering
        :param logger: logger for logging
        :param bq_client: the BigQuery client to use for streaming data. This defaults to creating
               a BigQuery client for you.
        """
        # Logging and statsd
        self.logger = logger
        self.stats = stats

        # Big Query client. Create a client if none is provided.
        self.client = bq_client if bq_client else bigquery.Client()

        # tags for ThreadStats
        self.bq_inserter_tags = ["source:bq"]

        # BQHelper collection
        self.bq_helpers_by_table_id = {}

    def register_helper(self, bq_helper: BQHelper) -> bool:
        """
        Adds a new BQHelper to the table of helpers and returns True...
        assuming it exists and it isn't already registered, otherwise, an error is logged and
        False is returned

        :param bq_helper: BQHelper instance
        :return: True if registration succeeds, else False
        """
        if not bq_helper:
            self.logger.warning("BQ Helper doesn't exist")
            self.stats.distribution(
                "errors",
                1,
                tags=self.bq_inserter_tags + ["reason:bq_helper_nonexistent"],
            )
            return False

        if bq_helper.table_id in self.bq_helpers_by_table_id:
            self.logger.warning(f"BQ Helper ({bq_helper.table_id}) already registered")
            self.stats.distribution(
                "errors",
                1,
                tags=self.bq_inserter_tags + ["reason:bq_helper_nonexistent"],
            )
            return False

        self.logger.debug(f"Registering BQHelper ({bq_helper.table_id})")
        self.bq_helpers_by_table_id[bq_helper.table_id] = bq_helper
        return True

    def register_new_helper(
        self, dataset_id: str, table_name: str, schema: BQSchema
    ) -> [BQHelper, None]:
        """
        Will create a new BQHelper instance and register it.
        If there's an error in registering the new helper, None is returned.
        Otherwise, the new BQHelper is returned.

        :param dataset_id: BQ Dataset ID
        :param table_name: BQ table name
        :param schema: BQSchema instance
        :return: the new registered BQHelper instance, or None if registration failed.
        """
        new_bq_helper = BQHelper.create_bq_helper(
            dataset_id, table_name, schema, self.stats, self.logger, self.client
        )
        registered = self.register_helper(new_bq_helper)
        if registered:
            return new_bq_helper

        return None

    def insert_all(self):
        """
        Will perform a default insert for every registered helper. If we have no registered helpers,
        we will just log a message and end.
        """
        if len(self.bq_helpers_by_table_id):
            self.logger.debug(
                f"Inserting into BigQuery for {len(self.bq_helpers_by_table_id)} "
                f"helpers"
            )
            for table_id, bq_helper in self.bq_helpers_by_table_id.items():
                self.logger.info(f"Inserting for BQ Helper {table_id} now")
                result = bq_helper.insert()
                self.logger.info(
                    f"Result from inserting for BQ Helper {table_id}: {result}"
                )
        else:
            self.logger.warning("There are no registered BQ Helpers fpr inserting")


class BQPubsubHelper(BQHelper):
    """
    A class that helps to collect information and insert to Big Query via Pubsub Subscription

    ## Awareness
    This helper is not threading-safe,
    so if you are in development with service using multiple threading,
    acknowledge this can help you save your time.

    ## Usage
    ```python
    # Import modules
    from bq_pubsub_helper import BQPubsubHelper, BQSchema, BQEntry

    # Define schema and default value
    # NOTE: value_type must be set if using this BQPubsubHelper.
    # If one type is added to value_type list, default_value must be set with the same type.
    # If null type is added, defaule_value is not needed.
    # If you don't want a entry to have null value (including "null", "None") as its value,
    # you can turn off the flag is_null_value_allowed=False
    # NOTE: This is schema is used to gather up information and define avro schema for payload transformation
    # Please create you `dataset.table` manualy (or terraform)
    schema = BQSchema([
        BQEntry(name="TracingId", value_type=["string"], default_value="null", is_rowid=True),
        BQEntry("StatusCode", value_type=["string"], default_value="default-code", is_null_value_allowed=False),
        BQEntry("ErrorMessage", value_type=["null", "string"])
    ])

    # Init BQPubsubHelper instance
    # NOTE: the BQSchema param is validated when initiating the BQPubsubHelper. If the schema is invalid,
    # the BQPubsubHelper instance will be automatically flagged as disabled. The methods can still
    # be called, but they will do nothing. It's best to explicitly validate the schema before we reach this point, though
    # NOTE: "statsd" can be set to None safely
    bq = BQPubsubHelper(statsd, logger, schema, pubsub_project_id, pubsub_topic_id)

    # Set props and insert to BigQuery via dedicated pubsub topic
    bq.set_prop("TracingId", "abc-def-ghi")
    bq.set_props({
        "StatusCode": "200",
        "ErrorMessage": None
    })
    bq_payload = bq.insert()

    # Or a shortcut
    bq.insert(props={
        "TracingId": "abc-def-ghi",
        "StatusCode": "200",
        "ErrorMessage": None
    })

    # Or if you want to use Log Router and Logging to export logs from Stackdriver to Bigquery:
    import logging
    logger = logging.getLogger('your.service.name')
    logging.info("Insert new record to BigQuery", extra=bq.bq_dict)

    ```
    """

    def __init__(
        self,
        statsd: [DogStatsd, None],
        logger: Logger,
        schema: BQSchema,
        enabled: bool = True,
        pubsub_project: str = "",
        pubsub_topic: str = "",
    ) -> None:
        super().__init__(statsd, logger, None, None, schema, enabled, "no_bq_client")

        # we dont use bq_client so this should be dereferenced
        self.client = None
        self.avro_schema = None

        # create the payload validator once the BQSchema is valid
        if self.enabled:
            self.avro_schema = {**self.schema.avro_schema}
            self.logger.debug(f"AVRO schema is created: {self.avro_schema}")
            # init avro serializer for payload transformer, it also validate the values of entries
            avro_schema = make_avsc_object(self.avro_schema, avro.schema.Names())
            self.serializer = AvroJsonSerializer(avro_schema)

        # create the pubsub publisher client
        self.bq_publisher = PublishToPubsub(
            logger=self.logger, project_id=pubsub_project, output_topic_id=pubsub_topic
        )

    def set_prop(self, key, value):
        """
        Set a key value to current payload
        Only set the value to the coresponding key if key name is allowed

        :param key:
        :param value:
        """
        if not self.enabled:
            return

        if key in self.schema.keys_where_set_prop_is_not_allowed:
            self.logger.error(f"{key} is not allowed to set via set_props method")
            if self.statsd:
                self.statsd.increment(
                    "errors", tags=["source:bq", "reason:invalid_schema"]
                )
            return
        if key not in self.schema.key_set:
            self.logger.error(f"Unknown key {key}")
            if self.statsd:
                self.statsd.increment(
                    "errors", tags=["source:bq", "reason:invalid_schema"]
                )
            return
        self.bq_dict = {**self.bq_dict, **{key: value}}

    def _payload_transformer(self, origin_payload: dict) -> Dict:
        """
        Transform a bq schema format object to avro format object

        :param origin_payload: payload need to be transformed to avro format
        :return: A dictionary with avro format

        """
        return json.loads(self.serializer.to_json(origin_payload))

    def insert(self, props=None) -> [Dict, None]:
        """
        If the helper is disabled, just clearing collected fields

        Insert our current payload (and additional properties specified here) by publishing
        to dedicated BQ pubsub topic

        If the insert is successful, we will return the payload representing what was inserted, and
        we will reset the payload to its default state. Otherwise, None is returned, which
        indicates something went wrong.

        :param props: an additional dictionary of key/value pairs to insert if specified -- defaults
               to no properties
        :return: payload inserted into BigQuery on success, or None if something went wrong, or
                 this helper is disabled
        """
        if not self.enabled:
            self.init_default_field_values()
            return None

        try:
            if props:
                self.set_props(props)

            if not self.is_null_values_valid():
                return None

            payload = self._payload_transformer(self.bq_dict)
            self.logger.debug(f"Transformer BQ pubsub payload: {payload}")

            self.bq_publisher.publish(data=payload)
            return payload
        except Exception as error:
            msg = f"There was an error when publishing payload to BigQuery Pubsub: {error},\
                  \nPayload: {self.bq_dict}"
            self.logger.error(msg, exc_info=error)
            if self.statsd:
                self.statsd.increment("errors", tags=["source:bq", "reason:unknown"])
            return None
        finally:
            self.init_default_field_values()

    def insert_all(self, props=None) -> [Dict, None]:

        return self.insert(self)


class PublishToPubsub:
    """
    A class to handle publishing the payload to a pubsub topic.
    """

    def __init__(self, logger: Logger, project_id: str = "", output_topic_id: str = ""):
        self.logger = logger
        # Client for Pubsub and default attributes
        self.publisher = PublisherClient()
        self.topic_path = self.publisher.topic_path(project_id, output_topic_id)

        if (self.topic_path.strip() and project_id and output_topic_id) in (
            "projects//topics/",
            "",
            None,
        ):
            error = f"Pubsub topic path is invalid: {self.topic_path}"
            self.logger.error(error)
            raise ValueError(error)

        # Configure the retry settings
        self.custom_retry = api_core.retry.Retry(
            initial=0.250,  # seconds (default: 0.1)
            maximum=90.0,  # seconds (default: 60.0)
            multiplier=1.45,  # default: 1.3
            predicate=api_core.retry.if_exception_type(
                api_core.exceptions.Aborted,
                api_core.exceptions.DeadlineExceeded,
                api_core.exceptions.InternalServerError,
                api_core.exceptions.ResourceExhausted,
                api_core.exceptions.ServiceUnavailable,
                api_core.exceptions.Unknown,
                api_core.exceptions.Cancelled,
            ),
        )

    def publish(self, data: dict):
        """
        Publishs payload to pubsub topic. Using either default project and topic
        or user defined project and topic.
        :param data: payload need to be published
        """
        message_bytes = json.dumps(data).encode("utf-8")
        future = self.publisher.publish(
            self.topic_path, message_bytes, retry=self.custom_retry
        )
        future.result()
        self.logger.debug(
            f"Payload is successfully published to BQ Pubsub topic: {self.topic_path}"
        )
