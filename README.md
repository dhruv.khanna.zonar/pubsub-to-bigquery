# Writing Directly from PubSub to BigQuery

Google has now launched direct streaming capability from Pub/Sub via “BigQuery Subscription”. In addition, Pub/Sub topic schemas provide the option of writing Pub/Sub messages to BigQuery tables with compatible schemas. If the schema is not enabled for your topic, messages will be registered as bytes or strings to the given BigQuery table.



## Getting started

Here is the step by step documentation to follow in order to implement this migration

[Documentation](https://zonarsystems.atlassian.net/wiki/spaces/DDOS/pages/137771189315/Dhruv+s+Internship+Project+Pub+Sub+-+BigQuery)


