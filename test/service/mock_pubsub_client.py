import pytest
import unittest.mock as mock


class MockPubsubClient:
    """
    A mock class for simulating a Pubsub client for testing purposes.
    """

    test_mock_publish_exception = Exception("Pubsub rejects this message")

    def __init__(self, mock_publish_exception: Exception = None):
        self.topic_path_value = ""
        self.mock_publish_exception = mock_publish_exception

    def topic_path(self, project_id, output_topic_id):
        self.topic_path_value = f"projects/{project_id}/topics/{output_topic_id}"
        return self.topic_path_value

    def publish(self, topic_path, message_bytes, retry):
        if self.mock_publish_exception:
            raise self.mock_publish_exception
        else:
            mock_result = mock.MagicMock()
            return mock_result


@pytest.fixture(scope="function")
def mock_happy_pubsub_client():
    """
    Test fixture to give a mock Pubsub client without any error
    """
    mock_client = MockPubsubClient()
    with mock.patch("big_query.PublisherClient", return_value=mock_client):
        yield mock_client


@pytest.fixture(scope="function")
def mock_pubsub_client_with_exception():
    """
    Test fixture for Pubsub client with exception
    """
    mock_client = MockPubsubClient(
        mock_publish_exception=MockPubsubClient.test_mock_publish_exception
    )
    with mock.patch("big_query.PublisherClient", return_value=mock_client):
        yield mock_client
