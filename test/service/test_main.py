import json
import pytest
import unittest.mock as mock
import time
from mock_pubsub_client import MockPubsubClient

class TestEvent:
    MOCK_TS = 123456789
    TEST_REQUEST_DATA = [
        {
            "event": "calibration",
            "vin": "abcdef",
            "status": 0,
            "serial_number": "1234567",
            "results": {"x": 1.001, "y": 1.213, "z": 2.765},
            "st": MOCK_TS,
        }
    ]


    @pytest.mark.parametrize(
            "test_request_data", [(d) for d in TEST_REQUEST_DATA],
        )
    def test_consume(self, test_request_data, modules_fixture, mock_request):
            with mock.patch(
            "bigquery.big_query.PublishToPubsub", return_value=MockPubsubClient()
        ):
            # Given
                test_request = mock_request(data=self._test_request(test_request_data))
                consumer = self._consumer(test_request, modules_fixture)
                msg_prov_module = modules_fixture["message_provider"]
                # When
                with mock.patch.object(
                    msg_prov_module.Calibration, "does_sn_exist_in_datastore"
                ) as mock_does_sn_exist_in_datastore, mock.patch.object(
                    msg_prov_module.Calibration, "publish",
                ) as mock_publish:
                    consumer.consume()
                    # Then
                    mock_does_sn_exist_in_datastore.assert_called_once()