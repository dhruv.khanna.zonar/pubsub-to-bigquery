terraform {
  backend "gcs" {
    # bucket name defined in backend_cfg
    prefix = "terraform/tf_calibration_notification_handler.state"
  }
}

locals {
  tf_stage = terraform.workspace
}

provider "google" {
  project     = var.tf_project
  region      = var.tf_region
}

provider "google-beta" {
  project     = var.tf_project
  region      = var.tf_region
}

data "terraform_remote_state" "general" {
  backend = "gcs"
  workspace = terraform.workspace
  config = {
    bucket  = var.state_bucket
    prefix  = "terraform/tf_general.state"
  }
}

module "calibration_notification_handler_bq_table" {
  source = "../modules/big_query"
  dataset_id = var.tf_bq_dataset_id
  table_id = var.tf_bq_table_id
}

module "bigquery_pubsubs" {
  source   = "./modules/pubsub_bigquery"
  
  tf_pubsub_bq_schema_id          = var.tf_pubsub_bq_schema_id
  tf_pubsub_bq_topic_id           = var.tf_pubsub_bq_topic_id
  tf_project                      = var.tf_project
  tf_dead_letter_pubsub_bq_id     = var.tf_dead_letter_pubsub_bq_id
  tf_pubsub_bq_subs_id            = var.tf_pubsub_bq_subs_id
  tf_bq_dataset_id                = var.tf_bq_dataset_id
  tf_bq_table_id                  = var.tf_bq_table_id

}

# CLOUD FUNCTION
module "calibration_notification_handler" {
  source = "../modules/cloud_function"

  name        = "calibration_notification_handler"
  description = "Function for receiving & processing RVD telegrams"

  tf_stage            = local.tf_stage
  tf_version          = var.tf_version
  # code_storage_bucket = data.terraform_remote_state.general.outputs.code_storage_bucket
  tf_vpc_connector    = "projects/${var.tf_project}/locations/us-central1/connectors/ld-tcu-connector"  # created in oem-terraform

  project = var.tf_project

  env = {
    GCP_PROJECT                 = var.tf_project
    RVD_SERVICE_DISCOVERY       = var.tf_rvd_url
    LOG_LEVEL                   = var.tf_log_level
    # DATADOG_API_KEY             = data.terraform_remote_state.general.outputs.datadog_api_key_name
    # DATADOG_APP_KEY             = data.terraform_remote_state.general.outputs.datadog_app_key_name
    ENVIRONMENT                 = var.datadog_environment
    PRODUCT                     = var.datadog_product
    SERVICE_NAME                = "calibration_notification_handler"
    RETRY_GOOGLE_APIS           = var.retry_google_apis
    BQ_DATASET_ID               = var.tf_bq_dataset_id
    BQ_TABLE_ID                 = var.tf_bq_table_id
    BQ_PUBSUB_TOPIC_ID          = var.tf_pubsub_bq_topic_id
  }

  event_trigger = [{
    event_type = "google.pubsub.topic.publish"
    resource   = "zonar_rdi_${var.datadog_environment}_${var.rvd_phone_home_receiver_to_calibration_pubsub}"
    failure_policy = {
      retry = true
    }
  }]

  status_codes = var.status_codes
}


# VARIABLES
variable "state_bucket" {
  type = string
  default = "default"
}

variable "tf_project" {
    type = string
    default = "rdi-dev"
}

variable "tf_version" {
    type = string
    default = "0.0.0-SNAPSHOT"
}

variable "tf_region" {
    type = string
    default = "us-west1"
}

variable "tf_log_level" {
    type = string
    description = "Cloud function log level"
    default = "INFO"
}

variable "datadog_product" {
    type = string
    description = "Product name for datadog tags"
    default = "rvd-data-inserter"
}

variable "datadog_environment" {
    type = string
    description = "Environment name for datadog tags"
    default = "dev"
}

variable "retry_google_apis" {
    type = bool
    description = "Controls whether or not to retry Google API calls"
    default = false
}

variable "status_codes" {
    type = list(string)
    description = "List of status codes for monitoring"
    default = ["203","400","401","403","404","413","500"]
}

variable "tf_rvd_url" {
    type = string
    description = "RVD URL (to sd.json for a stage)"
    default = "https://prelive.continental-rvd.de/sd.json"
}

variable "rvd_phone_home_receiver_to_calibration_pubsub" {
    type = string
    description = "Input ordered pubsub into harsh event receiver"
    default = "phone_home_receiver_to_calibration"
}

variable "tf_bq_dataset_id" {
  type = string
  description = "BigQuery Dataset for Binding Status Processor"
}

variable "tf_bq_table_id" {
  type = string
  description = "BigQuery Table name for Binding Status Processor"
}

variable "tf_pubsub_bq_topic_id" {
  type = string
  description = "PubSub Write to BigQuery Topic ID"
}

variable "tf_pubsub_bq_schema_id" {
    type = string
}

variable "tf_dead_letter_pubsub_bq_id" {
    type = string
}

variable "tf_pubsub_bq_subs_id" {
    type = string
}