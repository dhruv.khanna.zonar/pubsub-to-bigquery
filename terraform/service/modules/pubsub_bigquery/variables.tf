variable "tf_pubsub_bq_schema_id" {
    description = "Name of the Pub/Sub schema that is attached to the pubsub topic"
    type = string
}

variable "tf_pubsub_bq_topic_id" {
    description = "Name of the topic to which Pub/Sub will send the messages"
    type = string
}

variable "tf_project" {
    description = "Name of Google Cloud Project where the table is stored"
    type = string
}

variable "tf_dead_letter_pubsub_bq_id" {
    description = "Name of the dead letter topic"
    type = string
}

variable "tf_pubsub_bq_subs_id" {
    description = "Name of the subscription that will receive the messages and write them to BigQuery"
    type = string
}

variable "tf_bq_dataset_id" {
    description = "The dataset id where the BigQuery table will be created"
    type = string
}

variable "tf_bq_table_id" {
    description = "Name of the table to be created"
    type = string
}

variable "pubsub_bq_schema_template_file_path" {
  description = "JSON file path that contains AVRO Schema JSON for pubsub subscription"
  type = string
  default = "avro_schema.json"
}