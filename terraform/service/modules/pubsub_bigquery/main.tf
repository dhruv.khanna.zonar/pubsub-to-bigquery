data "google_project" "project" {}

resource "google_pubsub_schema" "schema" {
  name = var.tf_pubsub_bq_schema_id
  type = "AVRO"
  definition = file(var.pubsub_bq_schema_template_file_path)
}

resource "google_pubsub_topic" "topic" {
  name    = var.tf_pubsub_bq_topic_id
  project = var.tf_project

  depends_on = [google_pubsub_schema.schema]
  schema_settings {
    schema = "projects/${var.tf_project}/schemas/${google_pubsub_schema.schema.name}"
    encoding = "JSON"
  }
}

resource "google_pubsub_topic" "dead_letter_topic" {
  name    = var.tf_dead_letter_pubsub_bq_id
  project = var.tf_project
  depends_on = [google_pubsub_topic.topic]
}

resource "google_pubsub_subscription" "dead_letter_subscription" {
  name  = var.tf_dead_letter_pubsub_bq_id
  topic = google_pubsub_topic.dead_letter_topic.name
  project = var.tf_project

  message_retention_duration = "604800s" // 7 days
  retain_acked_messages      = false
  enable_message_ordering    = false
  ack_deadline_seconds       = 300
  expiration_policy {
    ttl = "" // never expires
  }
}

resource "google_pubsub_subscription" "subscription" {
  name  = var.tf_pubsub_bq_subs_id
  topic = google_pubsub_topic.topic.name
  project = var.tf_project

  message_retention_duration = "604800s" // 7 days
  retain_acked_messages      = false
  enable_message_ordering    = false
  # ack deadline must be greater nomal process time for 1 message
  ack_deadline_seconds = 300
  retry_policy {
    minimum_backoff = "60s"
    maximum_backoff = "600s"
  }
  # Config biqquery table
  bigquery_config {
    table = "${var.tf_project}:${var.tf_bq_dataset_id}.${var.tf_bq_table_id}"
    use_topic_schema = true
    write_metadata = false
    drop_unknown_fields = true
  }

  dead_letter_policy {
    dead_letter_topic = "projects/${var.tf_project}/topics/${google_pubsub_topic.dead_letter_topic.name}"
    max_delivery_attempts = 10
  }

  depends_on = [google_pubsub_topic.topic, google_pubsub_schema.schema, google_project_iam_member.pubsub-bigquery-member]
}

resource "google_project_iam_member" "pubsub-bigquery-member" {
  project = var.tf_project
  for_each = toset([
    "roles/bigquery.metadataViewer",
    "roles/bigquery.dataEditor",
    "roles/pubsub.publisher",
    "roles/pubsub.subscriber"
  ])
  role = each.value
  member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com"
}